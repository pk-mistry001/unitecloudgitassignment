import 'package:app/size_config.dart';
import 'package:app/src/git_repo_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//    systemNavigationBarColor: Colors.blue, // navigation bar color
//    statusBarColor: CustomColor.primary_pink,
    statusBarColor: Colors.blue,
    statusBarBrightness: Brightness.light, // status bar color
  ));
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => new MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'Unite Git',
              theme: ThemeData(
                primaryColor: Colors.blue,
              ),
              home: GitRepoListScreen()
            );
          },
        );
      },
    );
  }
}
