import 'package:app/src/client/git_repo_client.dart';
import 'package:app/src/event/git_repo_events.dart';
import 'package:app/src/models/git_repo_item_dto.dart';
import 'package:app/src/models/git_repo_detail_response.dart';
import 'package:app/src/utils/base_model.dart';

class GitRepoRepository {
  final GitRepoClient client = GitRepoClient();

  Future<BaseModel<List<GitRepoItemDTO>>> getRepoList(
      GetRepoListEvent listEvent) async {
    return await client.getGitRepoList(listEvent);
  }

  Future<BaseModel<GitRepoDetailResponse>> getRepoDetail(
      GetRepoDetailEvent detailEvent) async {
    return await client.getGitRepoDetail(detailEvent);
  }
}
