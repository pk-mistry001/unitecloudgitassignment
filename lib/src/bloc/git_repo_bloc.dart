import 'dart:async';

import 'package:app/src/event/git_repo_events.dart';
import 'package:app/src/repository/git_repo_repository.dart';
import 'package:app/src/state/git_repo_state.dart';
import 'package:bloc/bloc.dart';

class GitRepoBloc extends Bloc<GitRepoEvents, GitRepoState> {
  final GitRepoRepository repository = new GitRepoRepository();

  GitRepoBloc(GitRepoState initialState) : super(initialState);

  @override
  Stream<GitRepoState> mapEventToState(GitRepoEvents event) async* {
    if (event is GetRepoListEvent) yield* _mapGetRepoList(event);
    else if (event is GetRepoDetailEvent) yield* _mapGetRepoDetail(event);
  }

  Stream<GitRepoState> _mapGetRepoList(
      GetRepoListEvent event) async* {
    yield LoadDataProgress();
    try {
      yield GitRepoListLoadSuccessState(await repository.getRepoList(event));
    } catch (e) {
      print("exception list${e.toString()}");
      yield DataLoadFailure();
    }
  }

  Stream<GitRepoState> _mapGetRepoDetail(
      GetRepoDetailEvent event) async* {
    yield LoadDataProgress();
    try {
      yield GitRepoDetailLoadSuccessState(await repository.getRepoDetail(event));
    } catch (e) {
      print("exception detail${e.toString()}");
      yield DataLoadFailure();
    }
  }
}
