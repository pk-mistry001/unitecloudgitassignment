import 'package:app/src/api/api_client.dart';
import 'package:app/src/models/git_repo_item_dto.dart';
import 'package:app/src/models/git_repo_detail_response.dart';
import 'package:app/src/utils/base_model.dart';
import 'package:app/src/utils/server_error_handle.dart';
import 'package:app/src/event/git_repo_events.dart';

class GitRepoClient {
  GitRepoClient();

  Future<BaseModel<List<GitRepoItemDTO>>> getGitRepoList(GetRepoListEvent event) async {
    List<GitRepoItemDTO> response ;
    try {
      response = await getMyApi().getGitRepoList(event.lastItemId);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return BaseModel()..setException(ServerError.withError(error: error));
    }
    return BaseModel()..data = response;
  }

  Future<BaseModel<GitRepoDetailResponse>> getGitRepoDetail(GetRepoDetailEvent event) async {
    GitRepoDetailResponse response ;
    try {
      response = await getMyApi().getGitRepoDetail(event.ownerName,event.repoName);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return BaseModel()..setException(ServerError.withError(error: error));
    }
    return BaseModel()..data = response;
  }

}

