
import 'package:app/src/models/git_repo_item_dto.dart';
import 'package:app/src/models/owner_dto.dart';
import 'package:app/src/models/git_repo_detail_response.dart';
import 'package:app/src/utils/constatnt.dart';
import 'package:jaguar_retrofit/jaguar_retrofit.dart';
import 'package:jaguar_serializer/jaguar_serializer.dart';
import 'package:http/io_client.dart';
import 'package:jaguar_resty/jaguar_resty.dart' as resty;
part 'api_client.jretro.dart';

@GenApiClient()
class MyApi extends ApiClient with _$MyApiClient {
  final resty.Route base;

  MyApi(this.base);

  @GetReq(path: "repositories")
  Future<List<GitRepoItemDTO>> getGitRepoList(@QueryParam("?since=") int id);

  @GetReq(path: "repos/:ownerName/:repoName")
  Future<GitRepoDetailResponse> getGitRepoDetail(@PathParam() String ownerName,@PathParam() String repoName);
}
MyApi getMyApi() {

  globalClient = IOClient();
  final repo =
  JsonRepo()
    ..add(GitRepoDetailResponseJsonSerializer())
    ..add(GitRepoItemDTOJsonSerializer())
    ..add(OwnerJsonSerializer()); //Add all Serializer here

  return MyApi(route('$API_BASE_URL'))..jsonConverter = repo;
}

