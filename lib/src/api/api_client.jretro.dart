// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_client.dart';

// **************************************************************************
// JaguarHttpGenerator
// **************************************************************************

abstract class _$MyApiClient implements ApiClient {
  final String basePath = "";
  Future<List<GitRepoItemDTO>> getGitRepoList(int id) async {
    var req = base.get.path(basePath).path("repositories").query("?since=", id);
    return req.go(throwOnErr: true).map(decodeList);
  }

  Future<GitRepoDetailResponse> getGitRepoDetail(
      String ownerName, String repoName) async {
    var req = base.get
        .path(basePath)
        .path("repos/:ownerName/:repoName")
        .pathParams("ownerName", ownerName)
        .pathParams("repoName", repoName);
    return req.go(throwOnErr: true).map(decodeOne);
  }
}
