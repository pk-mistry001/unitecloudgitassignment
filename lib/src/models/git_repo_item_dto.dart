import 'package:jaguar_serializer/jaguar_serializer.dart';
import 'owner_dto.dart';

part 'git_repo_item_dto.jser.dart';

class GitRepoItemDTO {
  int id;
  String node_id;
  String name;
  String full_name;
  bool private;
  Owner owner;
  String description;
  bool fork;
  String url;

  GitRepoItemDTO(
      {this.id,
        this.node_id,
        this.name,
        this.full_name,
        this.private,
        this.owner,
        this.description,
        this.fork,
        this.url,
        });


}
@GenSerializer()
class GitRepoItemDTOJsonSerializer extends Serializer<GitRepoItemDTO> with _$GitRepoItemDTOJsonSerializer{}



