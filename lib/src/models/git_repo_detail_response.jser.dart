// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'git_repo_detail_response.dart';

// **************************************************************************
// JaguarSerializerGenerator
// **************************************************************************

abstract class _$GitRepoDetailResponseJsonSerializer
    implements Serializer<GitRepoDetailResponse> {
  Serializer<Owner> __ownerJsonSerializer;
  Serializer<Owner> get _ownerJsonSerializer =>
      __ownerJsonSerializer ??= OwnerJsonSerializer();
  @override
  Map<String, dynamic> toMap(GitRepoDetailResponse model) {
    if (model == null) return null;
    Map<String, dynamic> ret = <String, dynamic>{};
    setMapValue(ret, 'id', model.id);
    setMapValue(ret, 'node_id', model.node_id);
    setMapValue(ret, 'name', model.name);
    setMapValue(ret, 'full_name', model.full_name);
    setMapValue(ret, 'private', model.private);
    setMapValue(ret, 'owner', _ownerJsonSerializer.toMap(model.owner));
    setMapValue(ret, 'description', model.description);
    setMapValue(ret, 'fork', model.fork);
    setMapValue(ret, 'url', model.url);
    setMapValue(ret, 'created_at', model.created_at);
    setMapValue(ret, 'updated_at', model.updated_at);
    setMapValue(ret, 'size', model.size);
    setMapValue(ret, 'stargazers_count', model.stargazers_count);
    setMapValue(ret, 'watchers_count', model.watchers_count);
    setMapValue(ret, 'language', model.language);
    setMapValue(ret, 'forks_count', model.forks_count);
    setMapValue(ret, 'open_issues_count', model.open_issues_count);
    setMapValue(ret, 'forks', model.forks);
    setMapValue(ret, 'open_issues', model.open_issues);
    setMapValue(ret, 'watchers', model.watchers);
    return ret;
  }

  @override
  GitRepoDetailResponse fromMap(Map map) {
    if (map == null) return null;
    final obj = GitRepoDetailResponse();
    obj.id = map['id'] as int;
    obj.node_id = map['node_id'] as String;
    obj.name = map['name'] as String;
    obj.full_name = map['full_name'] as String;
    obj.private = map['private'] as bool;
    obj.owner = _ownerJsonSerializer.fromMap(map['owner'] as Map);
    obj.description = map['description'] as String;
    obj.fork = map['fork'] as bool;
    obj.url = map['url'] as String;
    obj.created_at = map['created_at'] as String;
    obj.updated_at = map['updated_at'] as String;
    obj.size = map['size'] as int;
    obj.stargazers_count = map['stargazers_count'] as int;
    obj.watchers_count = map['watchers_count'] as int;
    obj.language = map['language'] as String;
    obj.forks_count = map['forks_count'] as int;
    obj.open_issues_count = map['open_issues_count'] as int;
    obj.forks = map['forks'] as int;
    obj.open_issues = map['open_issues'] as int;
    obj.watchers = map['watchers'] as int;
    return obj;
  }
}
