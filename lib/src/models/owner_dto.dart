import 'package:jaguar_serializer/jaguar_serializer.dart';

part 'owner_dto.jser.dart';

class Owner {
  String login;
  int id;
  String node_id;
  String avatar_url;
  String gravatar_id;
  String url;
  String type;
  bool site_admin;

  Owner(
      {this.login,
        this.id,
        this.node_id,
        this.avatar_url,
        this.gravatar_id,
        this.url,
        this.type,
        this.site_admin});
}

@GenSerializer()
class OwnerJsonSerializer extends Serializer<Owner> with _$OwnerJsonSerializer{}