// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'git_repo_item_dto.dart';

// **************************************************************************
// JaguarSerializerGenerator
// **************************************************************************

abstract class _$GitRepoItemDTOJsonSerializer
    implements Serializer<GitRepoItemDTO> {
  Serializer<Owner> __ownerJsonSerializer;
  Serializer<Owner> get _ownerJsonSerializer =>
      __ownerJsonSerializer ??= OwnerJsonSerializer();
  @override
  Map<String, dynamic> toMap(GitRepoItemDTO model) {
    if (model == null) return null;
    Map<String, dynamic> ret = <String, dynamic>{};
    setMapValue(ret, 'id', model.id);
    setMapValue(ret, 'node_id', model.node_id);
    setMapValue(ret, 'name', model.name);
    setMapValue(ret, 'full_name', model.full_name);
    setMapValue(ret, 'private', model.private);
    setMapValue(ret, 'owner', _ownerJsonSerializer.toMap(model.owner));
    setMapValue(ret, 'description', model.description);
    setMapValue(ret, 'fork', model.fork);
    setMapValue(ret, 'url', model.url);
    return ret;
  }

  @override
  GitRepoItemDTO fromMap(Map map) {
    if (map == null) return null;
    final obj = GitRepoItemDTO();
    obj.id = map['id'] as int;
    obj.node_id = map['node_id'] as String;
    obj.name = map['name'] as String;
    obj.full_name = map['full_name'] as String;
    obj.private = map['private'] as bool;
    obj.owner = _ownerJsonSerializer.fromMap(map['owner'] as Map);
    obj.description = map['description'] as String;
    obj.fork = map['fork'] as bool;
    obj.url = map['url'] as String;
    return obj;
  }
}
