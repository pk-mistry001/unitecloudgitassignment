// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'owner_dto.dart';

// **************************************************************************
// JaguarSerializerGenerator
// **************************************************************************

abstract class _$OwnerJsonSerializer implements Serializer<Owner> {
  @override
  Map<String, dynamic> toMap(Owner model) {
    if (model == null) return null;
    Map<String, dynamic> ret = <String, dynamic>{};
    setMapValue(ret, 'login', model.login);
    setMapValue(ret, 'id', model.id);
    setMapValue(ret, 'node_id', model.node_id);
    setMapValue(ret, 'avatar_url', model.avatar_url);
    setMapValue(ret, 'gravatar_id', model.gravatar_id);
    setMapValue(ret, 'url', model.url);
    setMapValue(ret, 'type', model.type);
    setMapValue(ret, 'site_admin', model.site_admin);
    return ret;
  }

  @override
  Owner fromMap(Map map) {
    if (map == null) return null;
    final obj = Owner();
    obj.login = map['login'] as String;
    obj.id = map['id'] as int;
    obj.node_id = map['node_id'] as String;
    obj.avatar_url = map['avatar_url'] as String;
    obj.gravatar_id = map['gravatar_id'] as String;
    obj.url = map['url'] as String;
    obj.type = map['type'] as String;
    obj.site_admin = map['site_admin'] as bool;
    return obj;
  }
}
