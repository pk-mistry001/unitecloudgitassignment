import 'package:jaguar_serializer/jaguar_serializer.dart';
import 'owner_dto.dart';

part 'git_repo_detail_response.jser.dart';

class GitRepoDetailResponse {
  int id;
  String node_id;
  String name;
  String full_name;
  bool private;
  Owner owner;
  String description;
  bool fork;
  String url;
  String created_at;
  String updated_at;
  int size;
  int stargazers_count;
  int watchers_count;
  String language;
  int forks_count;
  int open_issues_count;
  int forks;
  int open_issues;
  int watchers;

  GitRepoDetailResponse(
      {this.id,
        this.node_id,
        this.name,
        this.full_name,
        this.private,
        this.owner,
        this.description,
        this.fork,
        this.url,
        this.created_at,
        this.updated_at,
        this.size,
        this.stargazers_count,
        this.watchers_count,
        this.language,
        this.forks_count,
        this.open_issues_count,
        this.forks,
        this.open_issues,
        this.watchers,
      });

}

@GenSerializer()
class GitRepoDetailResponseJsonSerializer extends Serializer<GitRepoDetailResponse> with _$GitRepoDetailResponseJsonSerializer{}


