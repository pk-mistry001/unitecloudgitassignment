import 'package:app/src/models/git_repo_detail_response.dart';
import 'package:app/src/state/git_repo_state.dart';
import 'package:app/src/utils/common_methods.dart';
import 'package:app/src/utils/images.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:filesize/filesize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import 'bloc/git_repo_bloc.dart';
import 'event/git_repo_events.dart';

class GitRepoDetailScreen extends StatefulWidget {
  final ownerName;
  final repoName;

  GitRepoDetailScreen(this.ownerName, this.repoName);

  @override
  GitRepoDetailScreenState createState() =>
      GitRepoDetailScreenState(ownerName, repoName);
}

class GitRepoDetailScreenState extends State<GitRepoDetailScreen> {
  final ownerName;
  final repoName;

  GitRepoDetailScreenState(this.ownerName, this.repoName);

  GitRepoBloc _gitRepoBloc;
  GitRepoDetailResponse _gitRepoDetailData;

  bool isLoadingProgress = false;             // for displaying circular indicator while network call

  void initState() {
    super.initState();
    _gitRepoBloc = GitRepoBloc(LoadInProgress());
    _gitRepoBloc.add(GetRepoDetailEvent(ownerName, repoName));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("$ownerName/$repoName",
            style: TextStyle(color: const Color(0xff757575))),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black54),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: ModalProgressHUD(
          child: BlocListener<GitRepoBloc, GitRepoState>(
            cubit: _gitRepoBloc,
            listener: (context, state) {
              setState(() {
                if (state is LoadDataProgress) {
                  // set true while fetching data from network
                  isLoadingProgress = true;
                } else {
                  // set false after data get from network
                  isLoadingProgress = false;
                }
                if (state is GitRepoDetailLoadSuccessState) {
                  if (state.response != null) {
                    // assign data in object
                    _gitRepoDetailData = state.response.data;
                  } else {
                    // displaying toast if data is null
                    Fluttertoast.showToast(
                        msg: "Something went wrong !!",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        backgroundColor: Colors.red,
                        textColor: Colors.white,
                        fontSize: setHeight(16));
                  }
                }
              });
            },
            child: _gitRepoDetailData != null
                ? buildScreen(_gitRepoDetailData)
                : isLoadingProgress
                    ? Container()
                    : Container(
                        height: setHeight(110),
                        child: emptyMessage("No Detail found")),
          ),
          inAsyncCall: isLoadingProgress,
          progressIndicator: CircularProgressIndicator(),
        ),
      ),
    );
  }
}

Widget buildScreen(GitRepoDetailResponse _gitRepoDetailData) {
  return Container(
    width: double.infinity,
    height: double.infinity,
    child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              width: double.infinity,
              height: setHeight(300),
              padding: EdgeInsets.only(
                  top: setHeight(28),
                  left: setWidth(12),
                  right: setWidth(12),
                  bottom: setHeight(12)),
              decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(18),
                      bottomRight: Radius.circular(18))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: setWidth(110),
                          height: setHeight(110),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12)),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey[100], blurRadius: 6)
                              ]),
                          padding: EdgeInsets.all(2),
                          child: CachedNetworkImage(
                            imageUrl: _gitRepoDetailData.owner.avatar_url,
                            placeholder: (context, url) => CircularProgressIndicator(),
                            errorWidget: (context, url, error) => Icon(Icons.error),
                            imageBuilder: (context, imageProvider)=>Container(
                              decoration: BoxDecoration(
//                            shape: BoxShape.circle,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(12)),
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: imageProvider
                                      )),
                            ),
                          )),
                      Container(
                        margin: EdgeInsets.only(
                            left: setWidth(28), top: setHeight(8)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Text(
                                "Owner Name :",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: setHeight(14),
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              width: setWidth(200),
                              child: Text(
                                _gitRepoDetailData.owner.login,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: setHeight(16),
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                            SizedBox(
                              height: setHeight(20),
                            ),
                            Container(
                              child: Text(
                                "Repository :",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: setHeight(14),
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              width: setWidth(200),
                              child: Text(
                                _gitRepoDetailData.name,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: setHeight(16),
                                    fontWeight: FontWeight.w700),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      countBox("Star", "${NumberFormat.compactCurrency(
                        decimalDigits: 1,
                        symbol:
                        '', // if you want to add currency symbol then pass that in this else leave it empty.
                      ).format(_gitRepoDetailData.stargazers_count)}", Images.star_outline,),
                      Spacer(),
                      countBox("Watch", "${NumberFormat.compactCurrency(
                        decimalDigits: 1,
                        symbol:
                        '',
                      ).format(_gitRepoDetailData.watchers_count)}", Images.eye_outline,),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      countBox("Fork", "${NumberFormat.compactCurrency(
                        decimalDigits: 0,
                        symbol:
                        '',
                      ).format(_gitRepoDetailData.forks_count)}", Images.source_fork,),
                      Spacer(),//mZ9kY58V9dJar3Zn5eXF
                      countBox("Size", "${filesize(_gitRepoDetailData.size)}", Images.size,),
                    ],
                  ),
                ],
              )),
          Container(
            padding: EdgeInsets.only(
                left: setWidth(20), right: setWidth(20), top: setWidth(30)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                textWithTitle("Issues opened :", "${_gitRepoDetailData.open_issues_count} Issues opened"),
                textWithTitle("Created at :", "${formatDateMMMddyyy(_gitRepoDetailData.created_at)}"),
                textWithTitle("Description :", "${_gitRepoDetailData.description}")
              ],
            ),
          )
        ],
      ),
    ),
  );
}

// widget for displaying watcher count, start count, fork count and size
Widget countBox(String name, String count, String image){
  return Container(
    width: setWidth(158),
    height: setHeight(36),
    margin: EdgeInsets.only(
        top: setHeight(20), right: setWidth(8)),
    padding: EdgeInsets.only(left: setHeight(4)),
    decoration: BoxDecoration(
        border: Border.all(width: 2, color: Colors.white),
        borderRadius:
        BorderRadius.all(Radius.circular(12))),
    child: Row(
      children: <Widget>[
        Container(
          child: Image.asset(
            image,
            width: setWidth(26),
            height: setHeight(26),
            color: Colors.white,
          ),
        ),
        Spacer(),
        Container(
            alignment: Alignment.center,
            child: Text(
              name,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: setHeight(12)),
            )),
        Spacer(),
        Container(
            width: setWidth(68),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.lightBlue,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(12),
                    topRight: Radius.circular(12))),
            height: double.infinity,
            child: Text(
              count,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: setHeight(12)),
            )),
      ],
    ),
  );
}
// widget for displaying text with tile (used in created date,issued open and description )
Widget textWithTitle(String title, String text){
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(bottom: setHeight(8)),
        child: Text(
          title,
          style: TextStyle(
              color: Colors.black45, fontSize: setHeight(14)),
        ),
      ),
      Container(
        padding: EdgeInsets.only(bottom: setHeight(20)),
        width: double.infinity,
        child: Text(
          text,
          style: TextStyle(
              color: Colors.black54,
              fontWeight: FontWeight.bold,
              fontSize: setHeight(16)),
        ),
      )
    ],
  );

}

//  for displaying empty message if data is null or empty
Widget emptyMessage(String message) {
  return Container(
    height: setHeight(10),
    margin: EdgeInsets.only(
        top: setHeight(18), left: setWidth(8), right: setWidth(8)),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8)),
        boxShadow: [BoxShadow(color: Colors.grey[350], blurRadius: 10.0)]),
    child: Align(
      alignment: Alignment.center,
      child: Text(message,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: setHeight(17),
              color: Colors.blue)),
    ),
  );
}
