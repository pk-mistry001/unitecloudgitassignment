
import 'package:app/src/models/git_repo_item_dto.dart';
import 'package:app/src/models/git_repo_detail_response.dart';
import 'package:app/src/utils/base_model.dart';
import 'package:equatable/equatable.dart';

abstract class GitRepoState extends Equatable {
  const GitRepoState();

  @override
  List<Object> get props => [];
}

class LoadInProgress extends GitRepoState {}
class LoadDataProgress extends GitRepoState {}


class GitRepoListLoadSuccessState extends GitRepoState {
  final BaseModel<List<GitRepoItemDTO>> response;
  const GitRepoListLoadSuccessState([this.response]);
  @override
  List<Object> get props => [response];
  @override
  String toString() => 'GitRepoListLoadSuccess { Data: $response }';
}

class GitRepoDetailLoadSuccessState extends GitRepoState {
  final BaseModel<GitRepoDetailResponse> response;
  const GitRepoDetailLoadSuccessState([this.response]);
  @override
  List<Object> get props => [response];
  @override
  String toString() => 'GitRepoDetailLoadSuccess { Data: $response }';
}


class DataLoadFailure extends GitRepoState {}