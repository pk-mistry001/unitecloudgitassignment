import 'package:equatable/equatable.dart';

abstract class GitRepoEvents extends Equatable{
  @override
  List<Object> get props => [];
}
class GetRepoListEvent extends GitRepoEvents{
  final int lastItemId;

  GetRepoListEvent(this.lastItemId);
  @override
  String toString() => "$lastItemId";
    @override
  List<Object> get props => [int];
}
class GetRepoDetailEvent extends GitRepoEvents{
  final String ownerName;
  final String repoName;

  GetRepoDetailEvent(this.ownerName,this.repoName);
  @override
  String toString() => "$ownerName, $repoName";
  @override
  List<Object> get props => [int];
}
