import 'package:app/src/bloc/git_repo_bloc.dart';
import 'package:app/src/models/git_repo_item_dto.dart';
import 'package:app/src/repo_detail_screen.dart';
import 'package:app/src/state/git_repo_state.dart';
import 'package:app/src/utils/common_methods.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import 'event/git_repo_events.dart';

class GitRepoListScreen extends StatefulWidget {
  @override
  GitRepoListScreenState createState() => GitRepoListScreenState();
}

class GitRepoListScreenState extends State<GitRepoListScreen> {
  GitRepoBloc _gitRepoBloc;
  GitRepoState initialState;
  int listFirstItemId = 0;
  List<GitRepoItemDTO> repoList = [];
  bool isLoadingProgress = false;
  bool isProgressIndicatorInList = false;

  void initState() {
    super.initState();
    _gitRepoBloc = GitRepoBloc(LoadDataProgress());

    setState(() {
      isLoadingProgress = true;
    });
    isLoadingProgress = true;
    // calling event for fetching data for first time
    _gitRepoBloc.add(GetRepoListEvent(listFirstItemId));

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        titleSpacing: setWidth(30),
        title: Text("Unite Cloud",
            style: TextStyle(color: const Color(0xff757575))),
      ),
      body: SafeArea(
        child: ModalProgressHUD(
          child: BlocListener<GitRepoBloc, GitRepoState>(
            cubit: _gitRepoBloc,
            listener: (context, state) {
              if (state is LoadDataProgress) {
                // set true while fetching data from network
                if (repoList.length < 1) {
                  // center proress indicator set true for first time while fetching data from network
                  isLoadingProgress = true;
                }
              } else {
                // set false after data get from network
                isLoadingProgress = false;
              }
              if (state is GitRepoListLoadSuccessState) {
                repoList.addAll(state.response.data); // adding response to list
                setState(() {
                  // after fetching data set false for disabling list bottom indicator
                  isProgressIndicatorInList = false;
                });
              }
            },
            child: repoList.length > 0
                ? buildScreen()
                : isLoadingProgress
                    ? Container()
                    : Container(
                        height: setHeight(110),
                        child: emptyMessage("No Git repo list found")),
          ),
          inAsyncCall: isLoadingProgress,
          progressIndicator: CircularProgressIndicator(),
        ),
      ),
    );
  }

  // for generating list
  Widget buildScreen() {
    print("build screen");
    return Column(
      children: <Widget>[
        Expanded(
          child: new ListView.builder(
              itemCount: repoList.length,
              itemBuilder: (BuildContext context, int index) {
                if (index == repoList.length - 1) {
                  print("list length ${repoList.length}");
                  _gitRepoBloc
                      .add(GetRepoListEvent(repoList[repoList.length - 1].id));
                  return CupertinoActivityIndicator(
                    radius: 15,
                  );
                }
                return builderListScreen(repoList[index]);
              }),
        ),
      ],
    );
  }

  // card design for item in list
  Widget builderListScreen(GitRepoItemDTO data) {
    return Container(
        padding: EdgeInsets.only(
            right: setWidth(6), bottom: setHeight(6), top: setHeight(10)),
        margin: EdgeInsets.only(
            left: setWidth(12), right: setWidth(12), top: setHeight(8)),
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(18)),
            boxShadow: [BoxShadow(color: Colors.grey[300], blurRadius: 8)]),
        child: InkWell(
          onTap: () {
            toNavigateDetailPage(data.owner.login, data.name);
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    top: setHeight(6),
                    bottom: setHeight(6),
                    left: setWidth(12),
                    right: setWidth(8)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(color: Colors.grey[350], blurRadius: 6)
                            ]),
                        padding: EdgeInsets.all(2),
                        child: CachedNetworkImage(
                          imageUrl: data.owner.avatar_url,
                          placeholder: (context, url) => Icon(Icons.account_circle,size: 58,),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                          imageBuilder: (context,imageProvider) =>Container(
                            decoration: BoxDecoration(
                                color: Colors.lightBlue,
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  fit: BoxFit.contain,
                                  image: imageProvider
                                 )),
                          ),
                        )),
                    Container(
                        width: MediaQuery.of(context).size.width / 1.6,
                        margin: EdgeInsets.only(left: setWidth(24)),
                        padding: EdgeInsets.only(right: setWidth(12)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              data.owner.login ?? "",
                              style: TextStyle(
                                  fontSize: setHeight(13),
                                  color: Colors.black45),
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Text(
                              data.name ?? "",
                              style: TextStyle(
                                  fontSize: setHeight(16),
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                            SizedBox(
                              height: setHeight(4),
                            ),
                            Text(
                              data.description ?? "",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: setHeight(12),
                                  color: Colors.black54),
                            )
                          ],
                        )),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  // for navigation to detail page
  toNavigateDetailPage(String ownerName, String repoName) {
    Navigator.of(this.context).push(
      MaterialPageRoute(
          builder: (BuildContext context) =>
              GitRepoDetailScreen(ownerName, repoName)),
    );
  }

  // for displaying empty message if data is null or empty
  Widget emptyMessage(String message) {
    return Container(
      height: setHeight(10),
      margin: EdgeInsets.only(
          top: setHeight(18), left: setWidth(8), right: setWidth(8)),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8)),
          boxShadow: [BoxShadow(color: Colors.grey[350], blurRadius: 10.0)]),
      child: Align(
        alignment: Alignment.center,
        child: Text(message,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: setHeight(17),
                color: Colors.blue)),
      ),
    );
  }
}
