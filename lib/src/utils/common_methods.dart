import 'package:intl/intl.dart';
import '../../size_config.dart';

// using for calculate width for reponsive ui
double setWidth(double width) {
  double finalWidth = (width / 4.1) * SizeConfig.blockSizeWidth;
  return finalWidth;
}
// using for calculate height for reponsive ui
double setHeight(double height) {
  double finalHeight = (height / 6.8) * SizeConfig.blockSizeHeight;
  return finalHeight;
}

// for date formatting
String formatDateMMMddyyy(String date){
  var formattedDate = DateFormat("MMM dd, yyyy").format(DateFormat("yyyy-MM-dd", "en_US").parse(date)) ;
  return formattedDate;
}