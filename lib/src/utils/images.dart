class Images{
  Images._();

  // using for accessing image
  static const String star_outline ="assets/images/star_outline.png";
  static const String eye_outline ="assets/images/eye_outline.png";
  static const String source_fork ="assets/images/source_fork.png";
  static const String size ="assets/images/size.png";
}