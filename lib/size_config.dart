// for responsive ui -- landscape or portrait and for different screen size of device

import 'package:flutter/widgets.dart';
class SizeConfig{
  static double screenWidth;
  static double screenHeight;
  static double blockSizeWidth = 0;
  static double blockSizeHeight = 0;

  static bool isPortrait = true;
  static bool isMobilePortrait = false;

  void init(BoxConstraints constraints, Orientation orientation) {
    if (orientation == Orientation.portrait) {
      screenWidth = constraints.maxWidth;
      screenHeight = constraints.maxHeight;
      isPortrait = true;
      if (screenWidth < 450) {
        isMobilePortrait = true;
      }
    } else {
      screenWidth = constraints.maxHeight;
      screenHeight = constraints.maxWidth;
      isPortrait = false;
      isMobilePortrait = false;
    }
    blockSizeWidth = screenWidth/100;
    blockSizeHeight = screenHeight/100;

  }
}